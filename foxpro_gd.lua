prod = true
local oldprint = print
print = function(...)
    -- if not prod or true then
    oldprint("[GD DEV] " .. ...)
    -- end
end
Root = require "moonloader"

local vkeys = require "vkeys"
local sampev = require "samp.events"
local moonad = require "MoonAdditions"
local api = require "foxpro.api"
local updater = require "foxpro.updater"
local settings = require "foxpro.settings"
local ui = require "foxpro.ui"
local chat = require "foxpro.chat"
local me = require "foxpro.me"
function sampev.onChatMessage(id, msg)
    chat.ProcessChatMessage(id, msg)
end
function sampev.onServerMessage(color, msg)
    chat.ProcessServerMessage(color, msg)
end
local mod = {}

local function _main()
    while not isOpcodesAvailable() or not isSampAvailable() do
        wait(100)
    end
    while not sampIsLocalPlayerSpawned() or sampIsDialogActive() or not isPlayerControllable(PLAYER_HANDLE) do
        wait(100)
    end
    settings.loadSettings()
    while not settings.loaded do
        wait(100)
    end
    sampAddChatMessage("GD ���������. ����������� ����� ������ �������� ������ F2", 0xFFF0B0)
    while not wasKeyPressed(vkeys.VK_F2) do
        wait(5)
    end
    printStringNow("Governemnt base by foxpro", 1350)

    if prod then
        print("[PROD]: GD started!")
    else
        print("[DEV]: GD started!")
    end
    updater.checkVersion()
    chat.load()
    api.load()
    local _, myID = sampGetPlayerIdByCharHandle(PLAYER_PED)
    local myName = sampGetPlayerNickname(myID)
    me.Name = myName
    sampSendChat("/showpass " .. myID)
    while true do
        wait(2)
        ui.processUi()
    end
    wait(-1)
end
local function onQuitGame()
    if doesDirectoryExist(getWorkingDirectory() .. "/imgs") then
        print("пїЅпїЅпїЅ")
    end
end
mod.main = _main
if not prod then
    -- delete on prod!
    function main()
        _main()
    end
end
return mod
