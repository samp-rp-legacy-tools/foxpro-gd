local mod = {}
local classes = require "foxpro.classes"

local chatCaptures = {}
function RegisterChatBlockParser(type, startString, colorStart, endString, colorEnd, callback)
    table.insert(
        chatCaptures,
        {
            type = type,
            startString = startString,
            colorStart = colorStart,
            endString = endString,
            colorEnd = colorEnd,
            callback = callback,
            capturing = false,
            accumulator = {}
        }
    )
end

function mod.ProcessServerMessage(color, message)
    for key, capture in pairs(chatCaptures) do
        if capture.startString == message then
            print("������ ������ �����, " .. capture.startString)
            capture.capturing = true
        elseif capture.capturing then
            if capture.endString == message then
                capture.capturing = false
                print("��������� ������ �����, " .. capture.endString)
                capture.callback(capture.accumulator)
                capture.accumulator = {}
            else
                table.insert(capture.accumulator, message)
                print("� ����������� ��������� ������ " .. message)
            end
        end
    end
end

function mod.ProcessChatMessage(id, message)
end

local function trim1(s)
    return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local chatEvents = {
    PASSPORT = "passport",
    CARPASS = "carpass",
    LICENSES = "licenses",
    MEDICALCARD = "medcard",
    WEAPON_SKILLS = "weapon_skills",
    JOB_SKILLS = "job_skills"
}

local events = {}
function mod.On(eventName, callback)
    if events[eventName] == nil then
        events[eventName] = {}
    end
    table.insert(events[eventName], {eventName = eventName, callback = callback})
    print("�������� ����� ������� ��� ���� �� " .. eventName)
    print("������ ������� ������� ����� " .. table.getn(events))
end
local function invokeCallbacksForEvent(name, param)
    print("�������� ������� �������� ��� ���-������: " .. name)
    local _c = 0
    if (events[name] ~= nil) then
        for i, v in ipairs(events[name]) do
            v.callback(param)
            _c = _c + 1
        end
    end
    print("��� ���-������ " .. name .. " ���� ������� " .. _c .. " ���������")
end

function mod.load()
    RegisterChatBlockParser(
        chatEvents.PASSPORT,
        "-----------===[ PASSPORT ]===----------",
        nil,
        "=============================",
        nil,
        function(acc)
            local name = trim1(string.gsub(acc[1], "���: ", ""))

            local years = tonumber(trim1(string.gsub(acc[2], "������� ��� � �����: ", "")))
            local married = trim1(string.gsub(acc[3], "����: ", ""))
            local faction = trim1(string.match(acc[4], "�����������: (.+) ���������"))
            local org_position = trim1(string.match(acc[4], "���������: (.+)"))
            local home = trim1(string.match(acc[5], "����� ����������: (.+)"))

            local person = classes.Person:new()
            person.Name = name
            person.Years = years
            person.Married = married
            person.OrgPosition = org_position
            person.HomeAddress = home
            person.Faction = faction
            print("�� ���� �������� ������ � ��� ��� ����� " .. name .. " " .. person.Name)
            invokeCallbacksForEvent(chatEvents.PASSPORT, person)
        end
    )
    RegisterChatBlockParser(
        chatEvents.CARPASS,
        "-----------===[ VEHICLE PASSPORT ]===----------",
        nil,
        "=====================================",
        nil,
        function(acc)
            local model = trim1(string.gsub(acc[1], "������: ", ""))

            local owner = trim1(string.gsub(acc[2], "��������: ", ""))
            local engine = tonumber(trim1(string.match(acc[3], "���������: (%d+) ���������")))

            local veh = classes.Vehicle:new()
            veh.Engine = engine
            veh.Owner = owner
            veh.Model = model
            print("�� ���� �������� ������ � ��� ��� ���� �� " .. model .. " " .. veh.Model)
            invokeCallbacksForEvent(chatEvents.CARPASS, veh)
        end
    )
end

mod.chatEvents = chatEvents
return mod
