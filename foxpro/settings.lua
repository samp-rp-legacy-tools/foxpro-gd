local encoding = require "lib.encoding"
encoding.default = "CP1251"
u8 = encoding.UTF8
local cjson = require "cjson"
local mod = {}
mod.loaded = false

function mod.loadSettings()
    local path = getWorkingDirectory() .. "/gd.settings.json"

    if doesFileExist(path) then
        local file = io.open(path, "r")
        local data = cjson.decode(file:read("*a"))
        mod.autoupdate = data.autoupdate
        mod.version = data.version
        mod.host = data.host
        mod.loaded = true
        print("Settings loaded from file: " .. tostring(mod.autoupdate) .. " " .. mod.version .. " " .. mod.host)
        io.close(file)
    else
        local file = io.open(path, "w")
        mod.autoupdate = true
        mod.version = "0.2"
        mod.loaded = true
        local data =
            cjson.encode(
            {
                autoupdate = true,
                version = mod.version,
                host = "http://gd.foxpro.su"
            }
        )
        file:write(data)
        io.close(file)
        print("Settings was generated")
    end
end
mod.version = "0.2"
return mod
