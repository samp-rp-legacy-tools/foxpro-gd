local mod = {}
local Person = {}
local Vehicle = {}
function Person:new()
    local obj = {}
    obj.Name = nil
    obj.Faction = nil
    obj.Years = nil
    obj.Faction = nil
    obj.Married = nil
    obj.OrgPosition = nil
    obj.SkinId = nil
    obj.HomeAddress = nil
    setmetatable(obj, self)
    self.__index = self;
    return obj
end
function Person:setName(name) self.name = name end
function Person:setFaction(faction) self.faction = faction end
function Person:setYears(years) self.years = years end
function Person:setMarriedStatus(married) self.married = married end
function Person:setOrgPosition(orgPosition) self.orgPosition = orgPosition end
function Person:setHomeAddress(homeAddress) self.homeAddress = homeAddress end

function Vehicle:new()
    local obj = {}
    obj.Model = nil
    obj.Owner = nil
    obj.Engine = nil
    setmetatable(obj, self)
    self.__index = self;
    return obj
end

mod.Person = Person
mod.Vehicle = Vehicle
return mod

