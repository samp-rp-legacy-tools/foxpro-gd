local copas = require "copas"
local cjson = require "cjson"
local http = require "copas.http"
local io = require "io"
local ltn12 = require "ltn12"

local encoding = require "encoding"
encoding.default = "CP1251"
local u8 = encoding.UTF8

local settings = require "foxpro.settings"
local ui = require "foxpro.ui"
local me = require "foxpro.me"
local chat = require "foxpro.chat"
local samp = require "lib.samp.events.handlers"
local chatEvents = require "foxpro.chat".chatEvents
local utils = require "foxpro.utils"

local mod = {}
function mod.load()
    chat.On(
        chatEvents.PASSPORT,
        function(person)
            local id = utils.sampGetPlayerIdByNickname(person.Name)
            print("ID ��������, ����������� ������� " .. id)
            local result, ped = sampGetCharHandleBySampPlayerId(id)
            print(tostring(result))
            if result then
                person.SkinId = getCharModel(ped)
            else
                person.SkinId = getCharModel(PLAYER_PED)
            end
            if person.Name == me.Name and not me.authed then
                me.Faction = person.Faction
                me.Married = person.Married
                me.HomeAddress = person.HomeAddress
                me.OrgPosition = person.OrgPosition
                me.Years = person.Years
                me.SkinId = person.SkinId
                print("���������� � ���� ���������")
                mod.auth()
            else
                mod.sendPersonInfo(person)
            end
        end
    )
    chat.On(
        chatEvents.CARPASS,
        function(vehicle)
            mod.sendVehicleInfo(vehicle)
        end
    )
end

function mod.auth()
    sampAddChatMessage("�����������...", 0xFFFFFF)
    local _, myID = sampGetPlayerIdByCharHandle(PLAYER_PED)
    local myName = sampGetPlayerNickname(myID)
    me:setName(myName)
    print(me.Name, me.Years, me.Married, me.Faction, me.OrgPosition, me.HomeAddress)
    mod.sendPersonInfo(me)
    me.authed = true
end
function mod.sendPersonInfo(person)
    lua_thread.create(
        function()
            print("���������� ���������� � ��������� " .. person.Name)

            local payload = cjson.encode(person)
            print(payload)
            local response_body = {}
            local url = settings.host .. "/api/person/add"
            print("������ ������ �� " .. url)
            local res, code =
                mod.httpRequest {
                url = settings.host .. "/api/person/add",
                method = "POST",
                headers = {
                    ["Content-Type"] = "application/json; cahrset=utf-8",
                    ["Content-Length"] = #payload
                },
                source = ltn12.source.string(payload),
                sink = ltn12.sink.table(response_body)
            }
            print(res, code)

            print("���������� ���������� � ��������� �� ������")

            local pedtype = getCharModel(PLAYER_PED)
            local skin_img = mod.getSkinImage(pedtype)
            print("got skin img in " .. skin_img)
        end
    )
end
function mod.sendVehicleInfo(veh)
    print("���������� ���������� � ���������� " .. veh.Owner .. " " .. veh.Model)
    lua_thread.create(
        function()
            local payload = u8:encode(cjson.encode(veh), "CP1251")
            print(payload)
            local response_body = {}
            local res, code =
                mod.httpRequest {
                url = settings.host .. "/api/vehicle/add",
                method = "POST",
                headers = {
                    ["Content-Type"] = "application/json; cahrset=utf-8",
                    ["Content-Length"] = #payload
                },
                source = ltn12.source.string(payload),
                sink = ltn12.sink.table(response_body)
            }
            print(res, code)
            print("���������� ���������� � ���������� �� ������")
        end
    )
end
function mod.getPersonInfo(name)
    local url = settings.host .. "/api/person/GetPersonByName?name=" .. name
    local response = mod.httpRequest(url)
    return cjson.decode(response)
end
function mod.getSkinImage(skinId)
    local url = settings.host .. "/static/images/skins/Skinid" .. skinId .. ".jpg"
    if not doesDirectoryExist(getWorkingDirectory() .. "\\imgs") then
        createDirectory(getWorkingDirectory() .. "\\imgs")
    end
    local fname = getWorkingDirectory() .. "\\imgs\\Skinid" .. skinId .. ".jpg"
    if doesFileExist(fname) then
        return fname
    end
    local body = http.request(url)
    local f = assert(io.open(fname, "wb"))
    f:write(body)
    f:close()

    return fname
end
function mod.getSkinImageMemory(skinId)
    local url = settings.host .. "/static/Skinid" .. skinId .. ".jpg"
    local body = http.request(url)
    return body
end
function mod.httpRequest(request, body, handler) -- copas.http
    -- start polling task
    if not copas.running then
        copas.running = true
        lua_thread.create(
            function()
                wait(0)
                while not copas.finished() do
                    local ok, err = copas.step(0)
                    if ok == nil then
                        error(err)
                    end
                    wait(0)
                end
                copas.running = false
            end
        )
    end
    -- do request
    if handler then
        return copas.addthread(
            function(r, b, h)
                copas.setErrorHandler(
                    function(err)
                        h(nil, err)
                    end
                )
                h(http.request(r, b))
            end,
            request,
            body,
            handler
        )
    else
        local results
        local thread =
            copas.addthread(
            function(r, b)
                copas.setErrorHandler(
                    function(err)
                        results = {nil, err}
                    end
                )
                results = table.pack(http.request(r, b))
            end,
            request,
            body
        )
        while coroutine.status(thread) ~= "dead" do
            wait(0)
        end
        return table.unpack(results)
    end
end

return mod
