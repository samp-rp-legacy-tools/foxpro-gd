local mod = {}

local api = require "foxpro.api"
local settings = require "foxpro.settings"
local dlstatus = require("moonloader").download_status

function mod.downloadUpdate()
    downloadUrlToFile(
        settings.host .. "/static/lua/foxpro_gd_all.luac",
        getWorkingDirectory() .. "/foxpro_gd_all.luac",
        function(id, status, p1, p2)
            if status == dlstatus.STATUS_ENDDOWNLOADDATA then
                print("��������� ����� ������ �������, ����� ���������������. ������������")
                reloadScripts()
            end
        end
    )
end
-- WIP
-- function mod.downloadLib()
--     downloadUrlToFile(settings.host..'/static/lua/foxpro_gd_all.luac',
--     getWorkingDirectory() .. '/foxpro_gd_all.luac',
--     function(id, status, p1, p2)
--        if status == dlstatus.STATUS_ENDDOWNLOADDATA then
--            print('��������� ����� ������ �������, ����� ���������������. ������������')
--            reloadScripts()
--        end
--     end)
-- end
function mod.checkVersion()
    local response = api.httpRequest(settings.host .. "/api/misc/version")
    print("��������� ������ �� �������: " .. response)
    if response then
        if not (settings.version == response) and settings.autoupdate then
            print("��������� ���������� �� ������ " .. response)
            mod.downloadUpdate()
        else
            print("����������� ��������� ������ ����������")
        end
    end
end
return mod
